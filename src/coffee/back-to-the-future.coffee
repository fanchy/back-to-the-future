# Set moment default language (as the App is in France)
# moment.locale('fr');

class Timer
	constructor: (selector) ->
		@selector = selector
		@present = selector.find('.time_present')

		if selector.hasClass('time_delay')
			@departed = selector.find('.time_departed')
			destination = selector.find('.time_destination')
			@destination = moment()
			@destination.year(parseInt(destination.find('.year').text()))
			@destination.month(destination.find('.month').text())
			@destination.date(parseInt(destination.find('.day').text()))
			@destination.hour(parseInt(destination.find('.hour').text()))
			@destination.minute(parseInt(destination.find('.min').text()))
			@destination.second(parseInt(destination.find('.sec').text()))


# Function that iterate every seconds so you can plug your own functions on it
iterateSeconds = ->
	now = moment()
	# console.log now

	# Put functions you need to iterate over seconds here
	updateClock(now)

	# Iterate every seconds here
	setTimeout iterateSeconds, 1000

leadingZero = (item) ->
	return if item < 10 then '0' + item else item

# Function that update DOM element to indicate current time
updateClock = (now) ->
	jQuery(timers).each (index, timer) ->
		timer.present.find('.hour').text(now.format('HH'));
		timer.present.find('.min').text(now.format('mm'));
		timer.present.find('.sec').text(now.format('ss'));
		timer.present.find('.day').text(now.format('DD'));
		timer.present.find('.month').text(now.format('MMM'));
		timer.present.find('.year').text(now.format('YYYY'));

		# If we're in countdown mode
		if timer.destination != undefined
			date_diff = if timer.destination >= now then timer.destination - now else now - timer.destination
			duration = moment.duration(date_diff)
			timer.departed.find('.hour').text(leadingZero(duration.hours()));
			timer.departed.find('.min').text(leadingZero(duration.minutes()));
			timer.departed.find('.sec').text(leadingZero(duration.seconds()));
			timer.departed.find('.day').text(leadingZero(duration.days()));
			timer.departed.find('.month').text(leadingZero(duration.months()));
			timer.departed.find('.year').text(leadingZero(duration.years()));

setFontSize = ($selector) ->
	if $selector.height() == $selector.width()
		_orientation = null
		orientation = 'square'
		maximum = $selector.height()
		coefficient = 1
	else if $selector.width() > $selector.height()
		_orientation = true
		orientation = 'landscape'
		maximum = $selector.height()
		coefficient = $selector.height() / $selector.width()
	else
		_orientation = false
		orientation = 'portrait'
		maximum = $selector.width()
		coefficient = $selector.width() / $selector.height()

	coefficient = Number.parseFloat(coefficient).toPrecision(2)
	ratio = maximum / 33.333
	ratio = Number.parseFloat(ratio).toPrecision(3)
	ratio = Math.floor(ratio)
	_return = orientation
	_return += ' : '

	if maximum == $selector.width()
		_return += '['

	_return += $selector.width()

	if maximum == $selector.width()
		_return += ']'

	_return += ' x '

	if maximum == $selector.height()
		_return += '['

	_return += $selector.height()

	if maximum == $selector.height()
		_return += ']'

	_return += ' ('
	_return += coefficient
	_return += ' / '
	_return += ratio
	_return += ' / '
	_return += $selector.width() * $selector.height()
	_return += 'px²'
	_return += ')'
	
	$selector.css 'font-size', ratio

	# console.log _return
	# jQuery('#js-debug li').text(_return);

# Set variables
moment.tz.setDefault 'Europe/Paris'
_orientation = undefined
orientation = undefined
coefficient = undefined
maximum = undefined
ratio = undefined
$selector = undefined
timers = []

# On window resize
jQuery(window).on 'resize', (e) ->
	setFontSize $selector

# On window load
jQuery(document).ready ($) ->
	$selector = $('.back-to-the-future')
	
	setFontSize $selector

	$selector.each (index, value) ->
		timer = new Timer($(value));
		timers.push(timer);
	
	iterateSeconds()
